﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLanguage : MonoBehaviour
{
   [SerializeField]
   LanguageObject languageObject = null;
   [SerializeField] string key = null;

   public void SelectLanguage(){
        if(this.GetComponent<Image>() != null){
            for(int i = 0; i < languageObject.ImageLanguage.Length; i++){
                if(i == PlayerPrefs.GetInt("CurrentLanguage")){
               this.GetComponent<Image>().sprite = languageObject.ImageLanguage[i];
           }

           if(this.GetComponent<Button>() != null){
               if(i == PlayerPrefs.GetInt("CurrentLanguage")){
                SpriteState spriteState = new SpriteState();
                spriteState.pressedSprite = languageObject.PressImage[i];
                this.GetComponent<Button>().spriteState = spriteState;
                }
            }           
            }    
        
        }

        if(this.GetComponent<SpriteRenderer>() != null){
            for(int i = 0; i < languageObject.ImageLanguage.Length; i++){
                if(i == PlayerPrefs.GetInt("CurrentLanguage")){
                    this.GetComponent<SpriteRenderer>().sprite = languageObject.ImageLanguage[i];
                }
            }
        }
        
        if(this.GetComponent<Text>() != null){
            
            if(PlayerPrefs.GetString(key) == "3 เกม" || PlayerPrefs.GetString(key) == "3 games" || PlayerPrefs.GetString(key) == "3 游戏"){
                for(int i = 0; i < languageObject.Text.Length; i++){
                    if(i == PlayerPrefs.GetInt("CurrentLanguage")){
                        this.GetComponent<Text>().text = languageObject.Text[i];
                        return;
                    }
                }
            }else this.GetComponent<Text>().text = " ";

            if(key == ""){
                for(int i = 0; i < languageObject.Text.Length; i++){
                    if(i == PlayerPrefs.GetInt("CurrentLanguage")){
                        this.GetComponent<Text>().text = languageObject.Text[i];
                        return;
                    }
                }
            }
        }
    }
}
