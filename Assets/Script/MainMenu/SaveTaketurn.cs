﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveTaketurn : MonoBehaviour
{
    float sce,min,hours,rawTime;

    string debugTime;
    [SerializeField] Text Timetext;

    private void Update() {
        CurrentTime();
        Timetext.text = debugTime;
    }

    void CurrentTime(){
        rawTime = Time.timeSinceLevelLoad;
        hours = (int)(rawTime / 3600);
        min = (int)(rawTime / 60);
        sce = (int)(rawTime % 60);
        debugTime = hours.ToString("00") + ":" + min.ToString("00") + ":" + sce.ToString("00");
    }

    void SaveDate(){
        PlayerPrefs.SetString("TaketurnDate1",System.DateTime.Now.ToString("dd/MM/yyyy"));
        PlayerPrefs.SetString("TaketurnTime1",debugTime);
        PlayerPrefs.SetString("Taketurngames1","3 games");
    }

    public void UpdateData(){
        PlayerPrefs.SetString("TaketurnDate3",PlayerPrefs.GetString("TaketurnDate2"));
        PlayerPrefs.SetString("TaketurnTime3",PlayerPrefs.GetString("TaketurnTime2"));
        PlayerPrefs.SetString("Taketurngames3",PlayerPrefs.GetString("Taketurngames2"));
        PlayerPrefs.SetString("TaketurnDate2",PlayerPrefs.GetString("TaketurnDate1"));
        PlayerPrefs.SetString("TaketurnTime2",PlayerPrefs.GetString("TaketurnTime1"));
        PlayerPrefs.SetString("Taketurngames2",PlayerPrefs.GetString("Taketurngames1"));

        SaveDate();
    }
}
