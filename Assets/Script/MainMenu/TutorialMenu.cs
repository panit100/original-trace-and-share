﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class TutorialMenu : MonoBehaviour
{
    [SerializeField] VideoPlayer TutorialVideo;
    public void PlayTutorialVideo(){
        TutorialVideo.Stop();
        TutorialVideo.Play();
    }
}

