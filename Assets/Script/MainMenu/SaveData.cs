﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : MonoBehaviour
{
    public static float startTimestamp;
    bool isActive = false;
    [SerializeField] string nameStage;
    float hours,minutes,seconds;
    string debugTime;
    float elaspedTime;


    public void Activate(){
        isActive = true;

        if(isActive){
            GetElaspedTime();
            saveTime();
            isActive = !isActive;
        }
    }

    public void StartTimeStamp(){
        startTimestamp = Time.time;
    }

    void GetElaspedTime(){
        elaspedTime = Time.time - startTimestamp;
        hours  = (int)(elaspedTime / 3600f);
        minutes = (int)(elaspedTime / 60f);
        seconds = (int)(elaspedTime % 60f);
        debugTime =  hours.ToString("00") + ":" + minutes.ToString("00") + ":" + seconds.ToString("00");
        
    }

    public void saveTime(){

        if(elaspedTime < PlayerPrefs.GetFloat(nameStage + "BestTime") || PlayerPrefs.GetFloat(nameStage + "BestTime") <= 0f){
            PlayerPrefs.SetFloat(nameStage + "SaveTime", elaspedTime);
            PlayerPrefs.SetFloat(nameStage + "BestTime",elaspedTime);
            PlayerPrefs.SetString(nameStage + "SaveBestTime", debugTime);
            PlayerPrefs.SetString(nameStage + "SaveBestDate", System.DateTime.Now.ToString("dd/MM/yyyy"));
            PlayerPrefs.SetString(nameStage + "SaveLastTime", debugTime);
            PlayerPrefs.SetString(nameStage + "SaveLastDate", System.DateTime.Now.ToString("dd/MM/yyyy"));
        }else if(elaspedTime > PlayerPrefs.GetFloat(nameStage + "BestTime"))
        {
            PlayerPrefs.SetFloat(nameStage + "SaveTime", elaspedTime);
            PlayerPrefs.SetString(nameStage + "SaveLastTime", debugTime);
            PlayerPrefs.SetString(nameStage + "SaveLastDate", System.DateTime.Now.ToString("dd/MM/yyyy"));
        }else
        {
            PlayerPrefs.SetFloat(nameStage + "SaveTime", elaspedTime);
            PlayerPrefs.SetFloat(nameStage + "BestTime",elaspedTime);
            PlayerPrefs.SetString(nameStage + "SaveBestTime", debugTime);
            PlayerPrefs.SetString(nameStage + "SaveBestDate", System.DateTime.Now.ToString("dd/MM/yyyy"));
            PlayerPrefs.SetString(nameStage + "SaveLastTime", debugTime);
            PlayerPrefs.SetString(nameStage + "SaveLastDate", System.DateTime.Now.ToString("dd/MM/yyyy"));
        }
    }
    
}
