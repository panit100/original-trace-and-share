﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadTaketurn : MonoBehaviour
{
    [SerializeField]
    Text[] PlayDate = null;
    [SerializeField]
    Text[] IntervalAfter = null;
    [SerializeField]
    Text[] WaitingTime = null;

    void Update()
    {
        loadDataInitializeTaketurn();
    }
    
    void loadDataInitializeTaketurn(){
        PlayDate[0].text = PlayerPrefs.GetString("TaketurnDate1","01/01/2020");
        IntervalAfter[0].text = PlayerPrefs.GetString("Taketurngames1","-");
        WaitingTime[0].text = PlayerPrefs.GetString("TaketurnTime1","--.--.--");
        PlayDate[1].text = PlayerPrefs.GetString("TaketurnDate2","01/01/2020");
        IntervalAfter[1].text = PlayerPrefs.GetString("Taketurngames2","-");
        WaitingTime[1].text = PlayerPrefs.GetString("TaketurnTime2","--.--.--");
        PlayDate[2].text = PlayerPrefs.GetString("TaketurnDate3","01/01/2020");
        IntervalAfter[2].text = PlayerPrefs.GetString("Taketurngames3","-");
        WaitingTime[2].text = PlayerPrefs.GetString("TaketurnTime3","--.--.--");
    }
}
