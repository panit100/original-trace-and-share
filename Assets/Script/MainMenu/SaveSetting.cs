﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSetting : MonoBehaviour
{
    public string saveKey;
    
    
    public void saveSetting(){
        if(PlayerPrefs.GetInt(saveKey,1) == 1)
        PlayerPrefs.SetInt(saveKey,0);
        else PlayerPrefs.SetInt(saveKey,1);
    }

    
}
