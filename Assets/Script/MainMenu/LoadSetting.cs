﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadSetting : MonoBehaviour
{
    public AudioSource Music = null;
    public AudioSource SFX = null;



    public void loadSetting(){
        if(this.GetComponent<SaveSetting>() != null){
            if(PlayerPrefs.GetInt(this.GetComponent<SaveSetting>().saveKey,1) == 1){
                this.GetComponent<Image>().enabled = true;
                if(Music != null){
                    Music.Play();
                }

                if(SFX != null){
                    SFX.mute = false;
                }
            }else{ 
                this.GetComponent<Image>().enabled = false;
                if(Music != null){
                    Music.Stop();
                }

                if(SFX != null){
                    SFX.mute = true;
                }
            }   
        }
    }
}
