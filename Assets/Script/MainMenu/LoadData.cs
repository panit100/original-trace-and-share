﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadData : MonoBehaviour
{
    [SerializeField]
    Text[] BestTime = null;
    [SerializeField]
    Text[] LastTime = null;
    [SerializeField]
    Text[] BestDate = null;
    [SerializeField]
    Text[] LastDate = null;     

    // Update is called once per frame
    void Update()
    {
        
        LoadDataInitializeTime();
        
    }

    void LoadDataInitializeTime(){
        LastTime[0].text = PlayerPrefs.GetString("StraightSaveLastTime","--.--.--");
        BestTime[0].text = PlayerPrefs.GetString("StraightSaveBestTime","--.--.--");
        LastDate[0].text = PlayerPrefs.GetString("StraightSaveLastDate","01/01/2020");
        BestDate[0].text = PlayerPrefs.GetString("StraightSaveBestDate","01/01/2020");
        LastTime[1].text = PlayerPrefs.GetString("DiagonalsSaveLastTime","--.--.--");
        BestTime[1].text = PlayerPrefs.GetString("DiagonalsSaveBestTime","--.--.--");
        LastDate[1].text = PlayerPrefs.GetString("DiagonalsSaveLastDate","01/01/2020");
        BestDate[1].text = PlayerPrefs.GetString("DiagonalsSaveBestDate","01/01/2020");
        LastTime[2].text = PlayerPrefs.GetString("CurvesSaveLastTime","--.--.--");
        BestTime[2].text = PlayerPrefs.GetString("CurvesSaveBestTime","--.--.--");
        LastDate[2].text = PlayerPrefs.GetString("CurvesSaveLastDate","01/01/2020");
        BestDate[2].text = PlayerPrefs.GetString("CurvesSaveBestDate","01/01/2020");
        LastTime[3].text = PlayerPrefs.GetString("AlarmClockSaveLastTime","--.--.--");
        BestTime[3].text = PlayerPrefs.GetString("AlarmClockSaveBestTime","--.--.--");
        LastDate[3].text = PlayerPrefs.GetString("AlarmClockSaveLastDate","01/01/2020");
        BestDate[3].text = PlayerPrefs.GetString("AlarmClockSaveBestDate","01/01/2020");
        LastTime[4].text = PlayerPrefs.GetString("BallSaveLastTime","--.--.--");
        BestTime[4].text = PlayerPrefs.GetString("BallSaveBestTime","--.--.--");
        LastDate[4].text = PlayerPrefs.GetString("BallSaveLastDate","01/01/2020");
        BestDate[4].text = PlayerPrefs.GetString("BallSaveBestDate","01/01/2020");
        LastTime[5].text = PlayerPrefs.GetString("FanSaveLastTime","--.--.--");
        BestTime[5].text = PlayerPrefs.GetString("FanSaveBestTime","--.--.--");
        LastDate[5].text = PlayerPrefs.GetString("FanSaveLastDate","01/01/2020");
        BestDate[5].text = PlayerPrefs.GetString("FanSaveBestDate","01/01/2020");
        LastTime[6].text = PlayerPrefs.GetString("MatchingSaveLastTime","--.--.--");
        BestTime[6].text = PlayerPrefs.GetString("MatchingSaveBestTime","--.--.--");
        LastDate[6].text = PlayerPrefs.GetString("MatchingSaveLastDate","01/01/2020");
        BestDate[6].text = PlayerPrefs.GetString("MatchingSaveBestDate","01/01/2020");
        LastTime[7].text = PlayerPrefs.GetString("ColoringSaveLastTime","--.--.--");
        BestTime[7].text = PlayerPrefs.GetString("ColoringSaveBestTime","--.--.--");
        LastDate[7].text = PlayerPrefs.GetString("ColoringSaveLastDate","01/01/2020");
        BestDate[7].text = PlayerPrefs.GetString("ColoringSaveBestDate","01/01/2020");
    }

    public void ClearData(){
        PlayerPrefs.DeleteAll();

        PlayerPrefs.SetString("StragihtSaveLastTime","--.--.--");
        PlayerPrefs.SetString("StraightSaveBestTime","--.--.--");
        PlayerPrefs.SetString("StraightSaveLastDate","01/01/2020");
        PlayerPrefs.SetString("StraightSaveBestDate","01/01/2020");
        PlayerPrefs.SetString("DiagonalsSaveLastTime","--.--.--");
        PlayerPrefs.SetString("DiagonalsSaveBestTime","--.--.--");
        PlayerPrefs.SetString("DiagonalsSaveLastDate","01/01/2020");
        PlayerPrefs.SetString("DiagonalsSaveBestDate","01/01/2020");
        PlayerPrefs.SetString("CurvesSaveLastTime","--.--.--");
        PlayerPrefs.SetString("CurvesSaveBestTime","--.--.--");
        PlayerPrefs.SetString("CurvesSaveLastDate","01/01/2020");
        PlayerPrefs.SetString("CurvesSaveBestDate","01/01/2020");
        PlayerPrefs.SetString("AlarmClockSaveLastTime","--.--.--");
        PlayerPrefs.SetString("AlarmClockSaveBestTime","--.--.--");
        PlayerPrefs.SetString("AlarmClockSaveLastDate","01/01/2020");
        PlayerPrefs.SetString("AlarmClockSaveBestDate","01/01/2020");
        PlayerPrefs.SetString("BallSaveLastTime","--.--.--");
        PlayerPrefs.SetString("BallSaveBestTime","--.--.--");
        PlayerPrefs.SetString("BallSaveLastDate","01/01/2020");
        PlayerPrefs.SetString("BallSaveBestDate","01/01/2020");
        PlayerPrefs.SetString("FanSaveLastTime","--.--.--");
        PlayerPrefs.SetString("FanSaveBestTime","--.--.--");
        PlayerPrefs.SetString("FanSaveLastDate","01/01/2020");
        PlayerPrefs.SetString("FanSaveBestDate","01/01/2020");
        PlayerPrefs.SetString("MatchingSaveLastTime","--.--.--");
        PlayerPrefs.SetString("MatchingSaveBestTime","--.--.--");
        PlayerPrefs.SetString("MatchingSaveLastDate","01/01/2020");
        PlayerPrefs.SetString("MatchingSaveBestDate","01/01/2020");
        PlayerPrefs.SetString("ColoringSaveLastTime","--.--.--");
        PlayerPrefs.SetString("ColoringSaveBestTime","--.--.--");
        PlayerPrefs.SetString("ColoringSaveLastDate","01/01/2020");
        PlayerPrefs.SetString("ColoringSaveBestDate","01/01/2020");

        PlayerPrefs.SetString("TaketurnDate1","01/01/2020");
        PlayerPrefs.SetString("Taketurngames1","-");
        PlayerPrefs.SetString("TaketurnTime1","--.--.--");
        PlayerPrefs.SetString("TaketurnDate2","01/01/2020");
        PlayerPrefs.SetString("Taketurngames2","-");
        PlayerPrefs.SetString("TaketurnTime2","--.--.--");
        PlayerPrefs.SetString("TaketurnDate3","01/01/2020");
        PlayerPrefs.SetString("Taketurngames3","-");
        PlayerPrefs.SetString("TaketurnTime3","--.--.--");

        LoadDataInitializeTime();
    }
    
}
