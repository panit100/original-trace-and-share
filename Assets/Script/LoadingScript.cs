﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class LoadingScript : MonoBehaviour
{
    public string loadscene;
    AsyncOperation asyncOperation;
    [SerializeField] VideoPlayer video; 

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadScene());
    }


    IEnumerator LoadScene()
    {
        yield return null;

        asyncOperation = SceneManager.LoadSceneAsync(loadscene);
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            if (asyncOperation.progress >= 0.9f)
            {
                if(video != null){
                    video.loopPointReached += videoManager;
                    yield return null;
                }else{
                    asyncOperation.allowSceneActivation = true;                
                    yield return null;
                }
            }

            yield return null;
        }
    }


    void videoManager(VideoPlayer vp){
        asyncOperation.allowSceneActivation = true;
    }
}
