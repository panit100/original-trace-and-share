﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetColor : MonoBehaviour
{
    [SerializeField]
    Paint[] paint;
    Ray ray;
    RaycastHit hit;

    [SerializeField]
    Color SelectColor;
    
    [SerializeField]
    float SelectPos;
    [SerializeField]
    float UnSelectPos;


    void Update()
    {
        GetColorWithMouse();
    }

    void GetColorWithMouse(){
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        

        if(Input.GetMouseButton(0)){    
            if(Physics.Raycast(ray,out hit)){
                if(hit.collider.gameObject == gameObject){
                    if(hit.collider.gameObject.tag == "Paint")
                    transform.position = new Vector2(SelectPos,transform.position.y); 

                    foreach(Paint n in paint){
                        n.color = SelectColor;
                    }
                }else{
                    if(hit.collider.gameObject.tag == "Paint")
                    transform.position = new Vector2(UnSelectPos,transform.position.y); 
                }
            
            }
        }
    }

    void GetColorWithTouch(){
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
            ray = Camera.main.ScreenPointToRay(touch.position);
        
            if(touch.phase == TouchPhase.Began){    
                if(Physics.Raycast(ray,out hit)){
                    if(hit.collider.gameObject == gameObject){
                        if(hit.collider.gameObject.tag == "Paint")
                        transform.position = new Vector2(SelectPos,transform.position.y); 

                        foreach(Paint n in paint){
                            n.color = SelectColor;
                        }
                    }else{
                        if(hit.collider.gameObject.tag == "Paint")
                        transform.position = new Vector2(UnSelectPos,transform.position.y); 
                    }
            
                }
            }
        }
    }
}
