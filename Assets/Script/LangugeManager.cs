﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LangugeManager : MonoBehaviour
{
    public static int SendLanguageNumber = 1;
    [SerializeField]
    int LanguageNumber = 0;

    [SerializeField]
    GameObject[] LanguageButtons;

    [HideInInspector]
    public ChangeLanguage[] changeLanguages;
    public LoadSetting[] loadSettings;

    private void Start(){
        changeLanguages = GameObject.FindObjectsOfType<ChangeLanguage>();
        loadSettings = GameObject.FindObjectsOfType<LoadSetting>();
        Setlanguage();
        LoadSettingMenu();
    }

    

    public void SelectLanguageNumber(){
        SendLanguageNumber = LanguageNumber;
        PlayerPrefs.SetInt("CurrentLanguage",LanguageNumber);
        foreach(ChangeLanguage m in changeLanguages)
        {
            m.SelectLanguage();
        }
    }

    public void Setlanguage(){
        changeLanguages = GameObject.FindObjectsOfType<ChangeLanguage>();
        foreach(ChangeLanguage m in changeLanguages)
        {
            m.SelectLanguage();
        }
    }

    public void LoadButton(){
        foreach(GameObject n in LanguageButtons){
            n.SetActive(true);
        }
        LanguageButtons[PlayerPrefs.GetInt("CurrentLanguage")].SetActive(false);
    }

    public void LoadSettingMenu(){
        foreach(LoadSetting n in loadSettings){
            n.loadSetting();
            
        }
    }
}
