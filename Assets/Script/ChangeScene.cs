﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    
    
    [SerializeField]
    string nameGame = null;

    
    public void ChangeFirstScene()
    {
        StartCoroutine(LoadFirstSceneAsyncchronous());
    }

    IEnumerator LoadFirstSceneAsyncchronous()
    {
        string name = null;
        switch (nameGame)
        {
            case "Straight":
                name = "CountStraight";
                break;
            case "Diagonals":
                name = "CountDiagonals";
                break;
            case "Curves":
                name = "CountCurves";
                break;
            case "Drawing":
                name = "MainStageCircle";
                break;
            case "Matching":
                name = "Main";
                break;
            default:
                break;
        }
        if (Camera.main.aspect >= 1.8)
        {
            name = name + ".18_10";
            print(18);
        }
        else if(Camera.main.aspect >= 1.7)
        {
            name = name + ".16_9";
            print(16);
        }else if(Camera.main.aspect >= 1.3){
            name = name + ".4_3";
            print(4);
        }
        AsyncOperation operation = SceneManager.LoadSceneAsync(name);
        while (!operation.isDone)
        {
            yield return null;
        }
    }

    public void changeScene(string sceneName){
        SceneManager.LoadScene(sceneName);
    }
}
