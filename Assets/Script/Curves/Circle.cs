﻿using UnityEngine;

public class Circle
{
    public Vector2 center;
    public float radius;

    public Circle( Vector2 inputCenter, float inputRadius )
    {
        center = inputCenter;
        radius = inputRadius;
    }

    /// <summary>
    /// This constructor function creates a circle instance
    /// given three distinct points that lie on it.
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="midPoint"></param>
    /// <param name="endPoint"></param>
    public Circle( Vector2 startPoint, Vector2 midPoint, Vector2 endPoint )
    {
        //  From the circle equation:
        //      ax^2 + ay^2 + bx + cy + d = 0
        //  We can substitute three distinct points that lie on the circle
        //  to determine the equation coefficients: a, b, c and d
        //  by the properties of matrix determinant
        float a = startPoint.x * (midPoint.y - endPoint.y)
                - startPoint.y * (midPoint.x - endPoint.x)
                + (midPoint.x * endPoint.y)
                - (endPoint.x * midPoint.y);
        float b = ((startPoint.x * startPoint.x) + (startPoint.y * startPoint.y)) * (endPoint.y - midPoint.y)
                + ((midPoint.x * midPoint.x) + (midPoint.y * midPoint.y)) * (startPoint.y - endPoint.y)
                + ((endPoint.x * endPoint.x) + (endPoint.y * endPoint.y)) * (midPoint.y - startPoint.y);
        float c = ((startPoint.x * startPoint.x) + (startPoint.y * startPoint.y)) * (midPoint.x - endPoint.x)
                + ((midPoint.x * midPoint.x) + (midPoint.y * midPoint.y)) * (endPoint.x - startPoint.x)
                + ((endPoint.x * endPoint.x) + (endPoint.y * endPoint.y)) * (startPoint.x - midPoint.x);
        float d = ((startPoint.x * startPoint.x) + (startPoint.y * startPoint.y)) * (endPoint.x*midPoint.y - midPoint.x*endPoint.y)
                + ((midPoint.x * midPoint.x) + (midPoint.y * midPoint.y)) * (startPoint.x*endPoint.y - endPoint.x*startPoint.y)
                + ((endPoint.x * endPoint.x) + (endPoint.y * endPoint.y)) * (midPoint.x*startPoint.y - startPoint.x*midPoint.y);

        //  From those coefficients, we can determine the circle
        //  center and radius
        center = new Vector2(-b / (2 * a), -c / (2 * a));
        radius = Mathf.Sqrt(((b * b) + (c * c) - (4 * a * d)) / (4 * a * a));
    }

    /// <summary>
    /// This function computes an angle between the vector
    /// from the circle center to the given point and
    /// the X+ axis.
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public float GetAngle( Vector2 point )
    {
        Vector2 centerToPoint = point - center;
        float angle = Mathf.Atan2(centerToPoint.y, centerToPoint.x);
        return angle < 0 ? angle + 2 * Mathf.PI : angle;
    }

    /// <summary>
    /// This function computes a point that lies on this
    /// circle, given an angle measured from the X+ axis
    /// counter-clockwise.
    /// </summary>
    /// <param name="angle"></param>
    /// <returns></returns>
    public Vector2 GetPoint( float angle )
    {
        float x = radius * Mathf.Cos(angle) + center.x;
        float y = radius * Mathf.Sin(angle) + center.y;
        return new Vector2(x, y);
    }
}
