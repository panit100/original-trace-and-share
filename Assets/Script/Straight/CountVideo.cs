﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class CountVideo : MonoBehaviour
{
    [SerializeField] VideoPlayer video;
    public string NextScene;
    bool isDone = false;

    void Update()
    {
        if(!isDone){
            video.loopPointReached += CheckOver;     
            isDone = true;
        }
    }

    void CheckOver(VideoPlayer vp){
            SceneManager.LoadScene(NextScene);
    }
}
