﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class LanguageObject : ScriptableObject
{
    public Sprite[] ImageLanguage = null;
    public Sprite[] PressImage = null;
    public string[] Text = null;
}
