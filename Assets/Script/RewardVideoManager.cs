﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class RewardVideoManager : MonoBehaviour
{
    [SerializeField] GameObject VideoImage;
    [SerializeField] GameObject Video = null;
    [SerializeField] VideoPlayer RewardVideo;
    // Start is called before the first frame update
    void Start()
    {
        
            RewardVideo.Play();
            VideoImage.SetActive(false);

    }

    void Update(){
        RewardVideo.loopPointReached += ImageReward;
    }

    void ImageReward(VideoPlayer vp){
        VideoImage.SetActive(true);
        Video.SetActive(false);
    }

}
